# sonatype-demo-environment - docker-compose


![Docker Compose](https://img.stackshare.io/service/3136/docker-compose.png)

```mermaid
sequenceDiagram
GitHub Plugin ->> NVS Cloud Scanner: Sends a zip file to scan
NVS Cloud Scanner ->> AHC: Asks to run an actual scan
AHC ->> AHC: Runs a scan
AHC ->> User: Sends email with results
AHC ->> NVS Cloud Scanner: Confirms that email was sent
NVS Cloud Scanner ->> GitHub Plugin: Sends a message that email was sent
```