# sonatype-demo-environment

[![pipeline status](https://gitlab.com/michaelmworthington/sonatype-demo-environment/badges/master/pipeline.svg)](https://gitlab.com/michaelmworthington/sonatype-demo-environment/commits/master) 


* [Documentation](#documentation)

## Documentation

* [Docker Compose](docker-compose/README.md)
* [Kubernetes](kubernetes/README.md)

## Screenshots

* IQ Server - audit.log : https://help.sonatype.com/iqserver/configuring/logging-configuration/audit-log
* IQ Server - policy-violation.log : https://help.sonatype.com/iqserver/configuring/logging-configuration/policy-violation-log
* NXRM - audit.log : https://help.sonatype.com/repomanager3/configuration/auditing
* NXRM - Prometheus : TODO
* Others???

TODO: Text the use cases, maybe with headings

### Nexus Firewall

![Grafana Firewall](https://gitlab.com/michaelmworthington/sonatype-demo-environment/raw/master/doc/images/grafana-firewall.png)


### Nexus Lifecycle Scans

![Grafana Lifecycle Scans](https://gitlab.com/michaelmworthington/sonatype-demo-environment/raw/master/doc/images/grafana-scan-events.png)


### Nexus Lifecycle IDE Usage

<p align="center"><img src="https://gitlab.com/michaelmworthington/sonatype-demo-environment/raw/master/doc/images/grafana-ide.png" alt="Nexus IQ for IDE Stats" /></p>

![Grafana Repository Prometheus](https://gitlab.com/michaelmworthington/sonatype-demo-environment/raw/master/doc/images/grafana-nxrm-prometheus.png)

![Grafana Platform Authentication](https://gitlab.com/michaelmworthington/sonatype-demo-environment/raw/master/doc/images/grafana-authentication-events.png)
